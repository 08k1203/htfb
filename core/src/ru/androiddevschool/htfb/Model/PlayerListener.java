package ru.androiddevschool.htfb.Model;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import ru.androiddevschool.htfb.Screens.Play;

/**
 * Created by Admin on 18.05.2017.
 */

public class PlayerListener extends ClickListener {
    public Play screen;
    private int id;
    public PlayerListener(Play screen, int id){
        super();
        this.screen = screen;
        this.id = id;
    }
    public boolean touchDown (InputEvent event, float x, float y, int pointer, int button){
        screen.touched(id);
        return super.touchDown(event,x,y,pointer,button);

    }
    public void touchUp (InputEvent event, float x, float y, int pointer, int button){
        super.touchUp(event,x,y,pointer,button);
        screen.lost(id);
    }
}
