package ru.androiddevschool.htfb.Model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g3d.particles.influencers.RegionInfluencer;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

import ru.androiddevschool.htfb.Utils.Assets;
import ru.androiddevschool.htfb.Utils.Values;

/**
 * Created by Admin on 18.05.2017.
 */

public class TimerLabel extends Image {
    private float time;
    private Label label;
    private boolean running;

    public TimerLabel(TextureRegionDrawable img) {
        super(img);
        label = new Label("label", new Label.LabelStyle(Assets.get().fonts.get("timer"), Color.PINK));
        stop();
    }

    public void updateLabel(){
        label.setText(String.format("%2d:%02d:%02d:%02d", (int)(time/60/60), (int)(time/60%60), (int)(time%60), (int)(time*100%100)));
        label.setPosition(Values.WORLD_WIDTH/2-180, Values.WORLD_HEIGHT/2, Align.center);
    }

    public void act(float delta) {
        super.act(delta);
        //System.out.println(time);
        if (running) {
            time += delta;
            updateLabel();
        }
    }

    public void draw(Batch batch, float parentAlpha){
        super.draw(batch,parentAlpha);
        label.draw(batch, parentAlpha);
    }

    public void start() {
        running = true;
    }
    public void stop() {
        time = 0;
        running = false;
        updateLabel();
    }
}
