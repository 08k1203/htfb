package ru.androiddevschool.htfb.Screens;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import ru.androiddevschool.htfb.HtFb;
import ru.androiddevschool.htfb.Model.PlayerListener;
import ru.androiddevschool.htfb.Model.TimerLabel;
import ru.androiddevschool.htfb.Utils.Assets;
import ru.androiddevschool.htfb.Utils.Values;
import ru.androiddevschool.std.Screens.StdScreen;

/**
 * Created by 08k1203 on 13.04.2017.
 */
public class Play extends StdScreen {

    public Play() {
        super();
        this.touched1 = false;
        this.touched2 = false;
    }

    @Override
    protected void initBg(Stage stage) {
        Image image = new Image(Assets.get().imgs.get("bg/uk"));
        image.setSize(Values.WORLD_WIDTH, Values.WORLD_HEIGHT);
        stage.addActor(image);

    }

    @Override
    protected void initWorld(Stage stage) {

    }

    @Override
    protected void initUi(Stage stage) {
        Table layout = new Table();
        layout.setFillParent(true);
        layout.setDebug(true);

        button1 = new Button(Assets.get().btnStyles.get("player1"));
        button1.addListener(new PlayerListener(this, 1));
        layout.add(button1).pad(50);

        timer = new TimerLabel(Assets.get().imgs.get("bg/tmr"));
        layout.add(timer).size(800,350).expandX();

        button2 = new Button(Assets.get().btnStyles.get("player2"));
        button2.addListener(new PlayerListener(this, 2));
        layout.add(button2).pad(50).row();

        stage.addActor(layout);
    }

    public void touched(int id){
        if (id == 1) touched1 = true;
        if (id == 2) touched2 = true;
        if (touched1 && touched2) timer.start();

    }
    public void lost(int id){
        timer.stop();
        if (touched1 && touched2){
            touched2 = false;
            touched1 = false;
            HtFb.get().setScreen(ScreenName.LOADING);
        }
    }

    public boolean touched1;
    public boolean touched2;
    public TimerLabel timer;
    Button button1, button2;
}
