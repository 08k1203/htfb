package ru.androiddevschool.htfb.Screens;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import ru.androiddevschool.htfb.Utils.Assets;
import ru.androiddevschool.htfb.Utils.Values;
import ru.androiddevschool.std.Controller.ScreenTraveler;
import ru.androiddevschool.std.Screens.StdScreen;

/**
 * Created by Admin on 18.05.2017.
 */

public class BeforePlay extends StdScreen {
    @Override
    protected void initBg(Stage stage) {
    }

    @Override
    protected void initWorld(Stage stage) {

    }

    @Override
    protected void initUi(Stage stage) {
        Image image = new Image(Assets.get().imgs.get("bg/pr"));
        image.setSize(Values.WORLD_WIDTH, Values.WORLD_HEIGHT);
        image.addListener(new ScreenTraveler(ScreenName.PLAY));
        stage.addActor(image);
    }
}
