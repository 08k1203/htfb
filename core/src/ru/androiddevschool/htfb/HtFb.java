package ru.androiddevschool.htfb;

import ru.androiddevschool.htfb.Screens.BeforePlay;
import ru.androiddevschool.htfb.Screens.Play;
import ru.androiddevschool.std.StdGame;

import static ru.androiddevschool.std.Utils.Names.ScreenName.LOADING;
import static ru.androiddevschool.std.Utils.Names.ScreenName.PLAY;

/**
 * Created by 08k1203 on 13.04.2017.
 */
public class HtFb extends StdGame {
    private static HtFb ourInstance = new HtFb();
    public static HtFb get() { return ourInstance; }
    private HtFb() {}

    @Override
    public void create() {
        super.create();
        screens.put(LOADING, new BeforePlay());
        screens.put(PLAY, new Play());
        setScreen(LOADING);

    }
}
