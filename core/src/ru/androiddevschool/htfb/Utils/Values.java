package ru.androiddevschool.htfb.Utils;

import com.badlogic.gdx.Gdx;

/**
 * Created by 08k1203 on 13.04.2017.
 */
public class Values {
    public static final float WORLD_WIDTH = 1920;
    public static final float WORLD_HEIGHT = 1080;
    public static final float ppuX = 1f * Gdx.graphics.getWidth() / WORLD_WIDTH;
    public static final float ppuY = 1f * Gdx.graphics.getHeight() / WORLD_HEIGHT;
}
