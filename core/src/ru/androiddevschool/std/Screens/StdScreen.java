package ru.androiddevschool.std.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import java.util.HashMap;
import java.util.HashSet;

import ru.androiddevschool.std.Controller.ScreenTraveler;
import ru.androiddevschool.std.Utils.Names;

import static ru.androiddevschool.std.Utils.Global.batch;
import static ru.androiddevschool.std.Utils.Global.game;
import static ru.androiddevschool.std.Utils.Names.ScreenState.ENTER;
import static ru.androiddevschool.std.Utils.Names.ScreenState.EXIT;
import static ru.androiddevschool.std.Utils.Names.ScreenState.RUN;
import static ru.androiddevschool.std.Utils.Names.StageType.BG;
import static ru.androiddevschool.std.Utils.Names.StageType.UI;
import static ru.androiddevschool.std.Utils.Names.StageType.WORLD;
import static ru.androiddevschool.std.Utils.Values.WORLD_HEIGHT;
import static ru.androiddevschool.std.Utils.Values.WORLD_WIDTH;

/**
 * Created by Гриша on 04.05.2017.
 */
abstract public class StdScreen implements Screen, Names {
    protected HashMap<StageType, Stage> stages;
    protected ScreenTraveler traveler;
    protected ScreenState screenState;
    protected InputMultiplexer multiplexer;

    public StdScreen() {
        stages = new HashMap<StageType, Stage>();
        stages.put(BG, initStage(batch));
        stages.put(UI, initStage(batch));
        stages.put(WORLD, initStage(batch));
        initBg(stages.get(BG));
        initWorld(stages.get(UI));
        initUi(stages.get(WORLD));
        game.gameState = GameState.PAUSE;
    }

    private Stage initStage(Batch batch) {
        OrthographicCamera camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.setToOrtho(false);
        return new Stage(new StretchViewport(WORLD_WIDTH, WORLD_HEIGHT, camera), batch);
    }

    abstract protected void initBg(Stage stage);
    abstract protected void initWorld(Stage stage);
    abstract protected void initUi(Stage stage);

    protected void onEnter(){screenState = ScreenState.ENTER;}
    protected void onExit(){screenState = ScreenState.EXIT;}

    private Stage stage(StageType stageType) {
        return stages.get(stageType);
    }

    public void transferActors(HashSet<Actor> transfer, StageType stageType) {
        if (transfer == null || stages.get(stageType) == null) return;
        for (Actor actor : transfer) {
            stages.get(stageType).addActor(actor);
            if (actor.getName() != null) parseActor(actor);
        }
    }

    protected void parseActor(Actor actor) {
    }

    @Override
    public void show() {
        onEnter();
        game.gameState = GameState.RUN;

        multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(stage(UI));
        multiplexer.addProcessor(stage(WORLD));
        Gdx.input.setInputProcessor(multiplexer);
    }

    protected void act(float delta) {
        stage(UI).act(delta);
        stage(BG).act(delta);
        stage(WORLD).act(delta);
    }

    protected void postAct() {

    }

    protected void draw() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage(BG).draw();
        stage(WORLD).draw();
        stage(UI).draw();
    }

    @Override
    public void render(float delta) {
        act(delta);
        postAct();
        draw();
        if (screenState == ENTER && actionsEnded()) {screenState = RUN;}
        if (screenState == EXIT && actionsEnded()) { leave(); }
    }


    protected boolean actionsEnded() {
        for (Actor act : stage(WORLD).getActors()) if (act.getActions().size != 0) return false;
        for (Actor act : stage(UI).getActors()) if (act.getActions().size != 0) return false;
        for (Actor act : stage(BG).getActors()) if (act.getActions().size != 0) return false;
        return true;
    }

    public void leavingScreen(ScreenTraveler traveler) {
        onExit();
        this.traveler = traveler;
    }

    protected void leave() {
        traveler.travel();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {
        game.gameState = GameState.PAUSE;
        game.saveResult();
    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {
        game.gameState = GameState.EXIT;
        stage(WORLD).dispose();
    }

}