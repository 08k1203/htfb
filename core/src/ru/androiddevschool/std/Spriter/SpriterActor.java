package ru.androiddevschool.std.Spriter;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.brashmonkey.spriter.Data;
import com.brashmonkey.spriter.Player;
import com.brashmonkey.spriter.SCMLReader;

/**
 * Created by Гриша on 15.05.2017.
 */
public class SpriterActor extends Actor {
    private Player player;
    private Drawer drawer;
    /**
     * @param handle SCML-file
     * @param atlas atlas
     */
    public SpriterActor(FileHandle handle, TextureAtlas atlas){
        Data data = new SCMLReader(handle.read()).getData();
        Loader loader = new Loader(data, atlas);
        loader.load(handle.file());
        drawer = new Drawer(loader);
        player = new Player(data.getEntity(0));
    }

    /** Called when the actor's position has been changed. */
    protected void positionChanged () {
        player.setPosition(getX(), getY());
    }

    //TODO: сделать масштабирование, альфаканал, точку поворота
    /** Called when the actor's size has been changed. */
    protected void sizeChanged () {
    }

    /** Called when the actor's rotation has been changed. */
    protected void rotationChanged () {
        player.setAngle(getRotation());
    }

    public void act(float delta){
        super.act(delta);
        player.update();
        player.setPosition(getX(), getY());
    }

    public void draw(Batch batch, float parentAlpha){
        super.draw(batch,parentAlpha);
        drawer.draw(player);
    }

    public Player getPlayer() {
        return player;
    }
}
