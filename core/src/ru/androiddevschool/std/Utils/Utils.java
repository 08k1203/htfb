package ru.androiddevschool.std.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * Created by Admin on 11.05.2017.
 */

public class Utils {
    public static TextureRegionDrawable makeDrawable(FileHandle handle){
        return new TextureRegionDrawable(new TextureRegion(new Texture(handle)));
    }
    public static TextureRegionDrawable makeDrawable(String fileName){
        return makeDrawable(handle(fileName));
    }
    public static FileHandle handle(String fileName){
        return Gdx.files.internal(fileName);
    }
}
