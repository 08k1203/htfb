package ru.androiddevschool.std.Utils;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import ru.androiddevschool.htfb.HtFb;
import ru.androiddevschool.std.StdGame;

/**
 * Created by Гриша on 15.05.2017.
 */
public class Global {
    public static StdGame game = HtFb.get();
    public static SpriteBatch batch;
    public static ShapeRenderer renderer;
}
