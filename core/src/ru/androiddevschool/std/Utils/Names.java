package ru.androiddevschool.std.Utils;

/**
 * Created by Гриша on 04.05.2017.
 */
public interface Names {
    enum PlayerState{
        LIVE,
        DIE
    }
    enum GameState{
        RUN,
        PAUSE,
        LOADING,
        OVER,
        EXIT
    }
    enum ScreenName{
        LOADING,
        MENU,
        PLAY
    }
    enum ScreenState{
        ENTER,
        RUN,
        EXIT
    }
    enum StageType {
        WORLD,
        UI,
        BG
    }

}
