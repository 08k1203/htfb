package ru.androiddevschool.htfb.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import ru.androiddevschool.htfb.HtFb;

public class DesktopLauncher {
	public static void main (String[] arg) {
		System.setProperty("user.name", "EnglishWords");
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.height = 540;
		config.width = 960;
		new LwjglApplication(HtFb.get(), config);
	}
}
